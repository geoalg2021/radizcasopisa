% !TEX encoding = UTF-8 Unicode
\documentclass[a4paper]{article}

\usepackage{color}
\usepackage{url}
\usepackage[T2A]{fontenc} % enable Cyrillic fonts
\usepackage[utf8]{inputenc} % make weird characters work
\usepackage{graphicx}

\usepackage[english,serbian]{babel}
%\usepackage[english,serbianc]{babel} %ukljuciti babel sa ovim opcijama, umesto gornjim, ukoliko se koristi cirilica

\usepackage[unicode]{hyperref}
\hypersetup{colorlinks,citecolor=green,filecolor=green,linkcolor=blue,urlcolor=blue}

\usepackage{listings}

%\newtheorem{primer}{Пример}[section] %ćirilični primer
\newtheorem{primer}{Primer}[section]

\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}

\lstset{ 
  backgroundcolor=\color{white},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}; should come as last argument
  basicstyle=\scriptsize\ttfamily,        % the size of the fonts that are used for the code
  breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
  breaklines=true,                 % sets automatic line breaking
  captionpos=b,                    % sets the caption-position to bottom
  commentstyle=\color{mygreen},    % comment style
  deletekeywords={...},            % if you want to delete keywords from the given language
  escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
  extendedchars=true,              % lets you use non-ASCII characters; for 8-bits encodings only, does not work with UTF-8
  firstnumber=1000,                % start line enumeration with line 1000
  frame=single,	                   % adds a frame around the code
  keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
  keywordstyle=\color{blue},       % keyword style
  language=Python,                 % the language of the code
  morekeywords={*,...},            % if you want to add more keywords to the set
  numbers=left,                    % where to put the line-numbers; possible values are (none, left, right)
  numbersep=5pt,                   % how far the line-numbers are from the code
  numberstyle=\tiny\color{mygray}, % the style that is used for the line-numbers
  rulecolor=\color{black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
  showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
  showstringspaces=false,          % underline spaces within strings only
  showtabs=false,                  % show tabs within strings adding particular underscores
  stepnumber=2,                    % the step between two line-numbers. If it's 1, each line will be numbered
  stringstyle=\color{mymauve},     % string literal style
  tabsize=2,	                   % sets default tabsize to 2 spaces
  title=\lstname                   % show the filename of files included with \lstinputlisting; also try caption instead of title
}

\begin{document}

\title{Revizija podeli-pa-vladaj tehnike za pronalaženje Voronoj dijagrama\\  \small{Prikaz naučnog rada u okviru kursa\\Geometrijski algoritmi\\ Matematički fakultet}}

\author{Marija Katić\\ katic.marija.97@gmail.com}


\maketitle

\abstract{
    Pred Vama je kratak prikaz naučnog rada \textit{Divide-and-conquer for Voronoi diagrams revisited} autora Oswin Aichholzer, Wolfgang Aigner, Franz Aurenhammer, Thomas Hackl, Bert Jüttle, Elisabeth Pilgerstorfer i Margot Rabl, objavljenog 2010. godine u naučnom časopisu \textit{Computational Geometry: Theory and Applications, ELSEVIER}. Autori su ugledni profesori na univerzitetima u Gracu i Lincu\footnote{Graz University of Technology, Institute for Theoretical Computer Science, Austria \par Graz University of Technology, Institute for Theoretical Computer Science, Austria \par Johannes Kepler University Linz, Institute of Applied Geometry, Austria}.
    \par
    Rad prezentuje novi algoritam za pronalaženje Voronoj dijagrama, koji problem svodi na pronalaženje medijalne ose\footnote{topološkog skeleta} pametno proširenog domena, izbegavajući direktno izračunavanje simetrala između sedišta i njihovih preseka, što mu za slučaj proizvoljnih dvodimenzionalnih sedišta, daje posebnu elegantnost implementacije, robusnost, računsku jednostavnost i numeričku stabilnost. 
    
}

\tableofcontents

\section{Uvod}
\label{sec:uvod}

Autori rad nazivaju \textit{revizijom} podeli-pa-vladaj tehnike jer je prvi algoritam koji je dao efikasno rešenje ovog čuvenog problema koristio tu tehniku, i zato što ta, kao i sve kasnije implementacije pomoću podeli-pa-vladaj tehnike nisu popularne u praksi. Zajedničko za sve te implementacije je komplikovana "odozdo na gore" \space faza - nalaženje preseka komplikovanih simetrala između sedišta i potreba za odbacivanjem prethodno sračunatih delova dijagrama. Negde su potrebne i prvobitne podele sedišta na jednostavnija radi računanja simetrala.
\par
Takođe, ostali poznati algoritmi, koji koriste tehnike kao što su \textit{inkrementalno umetanje} i \textit{brišuća prava}, pate od sličnih problema kada je u pitanju generalizacija na sedišta proizvoljnog oblika. I ako uzmemo u obzir poboljšanje koje se odnosi na izbegavanje direktnog izračunavanja preseka nađenih simetrala izračunavanjem centra kruga koji tangira tri "bliska" \space sedišta, ispostavlja se da nije rešen problem komplikovane implementacije u sedišta koja nisu tačke. Međutim, ovo unapređenje ima lepu osobinu direktnog izračunavanja nad sedištima umesto konstruisanja novih geometrijskih objekata nad kojima kasnije treba izvršavati izračunavanja. Tu ključnu prednost ima i algoritam koji autori prezentuju, sa razlikom što ona nije veštački ubačena kao pooljšanje, već dolazi prirodno zbog načina posmatranja problema.

\section{Posmatranje problema iz novog ugla i ključni uvidi}
\label{sec:postavka_problema}

Prvo precizirajmo šta je ulaz. Posmatramo Euklidsku ravan, a sedišta su, po parovima disjunktni, zatvoreni topološki diskovi dimenzije nula, jedan ili dva. Ovde se ubrajaju poligoni, kružni diskovi, splajn krive\footnote{Splajn je deo po deo polinomijalna funkcija.}, duži, tačke, i sl. Jedna od značajnijih prednosti ovog algoritma je što prirodno i gotovo bez povećanja složenosti radi sa splajn krivama, koje nadmašuju moć modelovanja poligonijalnim uz manju količinu ulaznih podataka. Postoji i prostor za proširenje na potpuno proizvoljna sedišta, razmatranjem glatkog nadovezivanja krivih (eng. \textit{biarc}). Autori, sa tim u vezi, kažu da je ovo možda prvi praktični algoritam koji se ponaša efikasno u slučaju proizvoljnih sedišta. 
\par
Zatim, kako definišu Voronoj dijagram? Voronoj dijagram je određen \textit{grafom ivica} (eng. edge graph) koji je definisan kao skup svih tačaka koje imaju više od jedne najbliže tačke u uniji svih tačaka sedišta. Primetimo da se čak i u defniciji Voronoj dijagrama izbegava posmatranje simetrala. Ovakva definicija ne daje ni prosotor za neprijatne pojave poput dvodimenzionalnih simetrala ili nepovezanog grafa, koje su česte u uopštenjima ranijih poznatih algoritama. Na slici \ref{fig} levo vidimo jedan primer Voronoj dijagrama za proizvoljna sedišta.
\par
Površinu koja sadrži sedišta možemo bez gubitaka na opštosti ograničiti. Posmatrajmo datu oblast kao ograničenu, povezanu mnogostrukost gde sedišta predstavljaju rupe. Ključni uvid je da je pomenuti graf zapravo medijalna osa te mnogostrukosti.

\begin{figure}
    \centering
    \begin{minipage}[t]{0.35\textwidth}
        \centering
        \includegraphics[width=\textwidth]{primer_voronoj_dijagrama.png}
    \end{minipage}
    %\hfill
    \begin{minipage}[t]{0.55\textwidth}
        \centering
        \includegraphics[width=\textwidth]{prosirivanje_domena.png}
    \end{minipage}
    \caption{Primer Voronoj dijagrama (levo) i proširena oblast sa orijentisanom granicom (desno)}
    \label{fig}
\end{figure}

\par
Izračunavanje medijalne ose nije ni malo jednostavno u slučaju oblasti koje nisu prosto povezane ("koje imaju rupe"). Zato se uvodi jedno proširenje ove oblasti, koje deli određenu osobinu sa prosto povezanim oblastima koja je dovoljna da se jedan postojeći algoritam pronalaženja medijalne ose za prosto povezane oblasti prilagodi ovom slučaju. Medijalna osa ovog proširenja će se od originalne razlikovati samo tako što će biti izbačeno određenih n tačaka. Njihovim izbacivanjem su, ispostavlja se, prekinuti svi ciklusi u originalnom grafu, i dobija se stablo, iz kojeg se vrlo jednostavno može rekonstruisati traženi graf (samo vraćanjem tih n tačaka).
\par
Kojih n tačaka sa medijalne ose će biti izbačeno? Za proizvoljno sedište s, ako je p(s) tačka sa najmanjom y koordinatom sedišta s, izbacujemo tačku q(s), najbližu tačku koja se nalazi na grafu, vertikalno ispod p(s). Kako ni jedan put onda ne može da se nastavi "ispod" \space nekog sedišta, graf bez ovih n tačaka nema ciklusa. Podsetimo, ove tačke ćemo naći na početku, bez ikakvog znanja o grafu - njega treba naći!
\par
Kako izvršavamo pomenuto zgodno proširenje oblasti? U pomenutih n \textit{tačaka preseka} ćemo datu oblast na neki način  "preseći". Želimo da oko tačke preseka imamo dva "kraja" \space oblasti u obliku kružnih lukova da bi nova medijalna osa bila kao stara samo tu presečena. Kružni lukovi najvećeg upisanog diska sa centom u toj tački će biti željeni krajevi oblasti. Formalno možemo dva diska "podići" \space na različite nivoe u prostoru, a rastjanje i dalje posmatrati kao euklidsko rastojanje projekcije na ravan (formalan matematički opis možete pronaći u radu). Neformalno, a nadam se dovoljno za razumevanje ideje, možemo posmatrati sliku \ref{fig} desno, isprekidane linije predstavljaju deo granice oblasti koji se nalazi ispod ravni papira.


\par
Primetimo da, kada se granica ove proširene oblasti obilazi u fiksiranoj orijentaciji, unutrašnjost uvek ostaje sa iste strane - to je najavljena, bitna, osobina, koju ovakva oblast deli sa prosto povezanim oblastima. Uzmimo sledeću primedbu kao zanimljivost: obilazak granice u fiksiranoj orijentaciji odgovara obilasku stabla (medijalne ose) u dubinu.


\section{Detalji implementacije}
\label{sec:detalji_implementacije}

Ostalo je da preciziramo kako naći tačke q(s), kako naći proširenu oblast, i kako zatim naći medijalnu osu te proširene oblasti.

\subsection{Pronalaženje granice proširene oblasti}
\label{subsec:pronalazenje_granice_prosirene_oblasti}

Granicu proširene oblasti je vrlo jednostavno naći kada imamo tačke q(s). Tačke q(s) se mogu naći jednostavno u $O(nlogn)$ pomoću algoritma koji koristi tehniku brišuće prave. Za svako sedište treba naći najveći krug čiji severni pol dodiruje to sedište u tački p(s) (najnižoj tački sedišta), a koji se može upisati u datu oblast (sedišta su rupe, da podsetimo). Centar tog kruga je tražena tačka q(s). Kada brišuća prava pređe preko neke p(s) ona postaje "aktivna" \space (traži joj se odgovarajuća q(s)). Kako brišuća prava ide niže, i detektuje susedne ivice levo i desno koje će smetati krugu sa polom u p(s) da bude širi (tj. detektuje tačke sa susednih sedišta horizontalno najbliže vertikali kroz p(s)), tako se apdejtuje veličina kruga sa severnim polom u p(s). Kada brišuća prava više ne preseca taj krug, p(s) se može deaktivirati - odgovarajući krug, tj. q(s) je pronađeno!

\subsection{Pronalaženje medijalne ose proširene oblasti}
\label{subsec:pronalazenje_medijalne_ose}

Ovde konačno stupa na scenu podeli-pa-vladaj tehnika. Nastavićemo da zamišljenu medijalnu osu (koju tek računamo) presecamo tačkama na manja stabla, tj. proširenu oblast, pomoću novih upisanih  diskova, delimo na već objašnjen način, do baznih slučajeva. Opet ćemo te tačke znati da sračunamo, naravno, bez posedovanja medijalne ose.
\par
Podsetimo da smo rekli da je u ovom algoritmu faza "odozdo na gore" \space vrlo jednostavna. I sada to i vidimo - sastoji se samo od vraćanja izbačenih tačaka koje spajaju sračunate delove grafa.
\par
Kako računati pomenute tačke? Ako nam je data tačka p sa ivice nekog sedišta, koju upisani krug treba da sadrži, krug računamo na sledeći način: smanjujemo ga sve dok preseca neko sedište. To se može izvesti u vremenu O(n).
\par
Kako biramo tačke p, tj lokacije gde želimo presek oblasti? Ovo se ispostavlja kao netrivijalno i autori nude rešenje da odabir ovih tačaka randomizujemo kako bismo imali efikasno prosečno vreme izvršavanja.

\subsection{Praktični aspekti}
\label{subsec:prakticni_aspekti}

Pojedinačni objekti od kojih se sastoji granica oblasti odgovaraju granama medijalne ose skoro obostrano jednoznačno. Tako, ako randomizujemo izbor tačke p, randomizujemo zapravo izbor ivice medijalne ose koju presecamo, i dobijamo efikasno ponašanje u srednjem slučaju.
\par
U slučaju da je broj sedišta mali u odnosu na n - broj komponenti kojima su sve zajedno opisane, dijametar grafa će biti linearno velik, i slučajnim odabirom tačke p, graf će biti podeljen na dva dela očekivane veličine $\Theta (n)$. 
\par
Autori zatim eksperimentalno utvrđuju, pomoću svoje implementacije, da očekivano vreme uz gorepomenuti uslov jeste $O(nlogn)$ i to uz male konstante. Takođe utvrđuju da je bez tog uslova, na primer u slučaju tačkastih sedišta, očekivano vreme $O(n \sqrt{n})$. To je cena koju plaćamo zarad jednostavnosti i opštosti ovog algoritma.

\section{Primene}
\label{sec:primene}


Rad se osvrće i na praktičnost algoritma prezentovanjem dve njegove zanimljive primene: planiranje kretanja robota i nalaženje ofseta date figure u ravni. Jednostavno se oba problema svode na problem pronalaženja Voronoj dijagrama, međutim ono što je ovde zanimljivo je da se ovaj algoritam posebno lepo ponaša kada su sedišta zadata u PC reprezentaciji\footnote{Ivice su deo-po-deo kružni lukovi.} (eng. piecewise circular representation) - izlaz (graf) se sastoji od nezanemarljivo manje ivica u poređenju sa drugim algoritmima, što na primer dovodi do kompaktnijeg zapisa putanje za robota.


\end{document}
