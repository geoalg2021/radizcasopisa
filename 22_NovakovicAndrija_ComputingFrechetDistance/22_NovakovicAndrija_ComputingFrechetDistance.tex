% !TEX encoding = UTF-8 Unicode

\documentclass[a4paper]{article}

\usepackage{color}
\usepackage{url}
\usepackage[T2A]{fontenc} % enable Cyrillic fonts
\usepackage[utf8]{inputenc} % make weird characters work
\usepackage{graphicx}
\usepackage{xcolor}
\usepackage{amssymb}
\usepackage{wrapfig}
\usepackage{subfig}
\usepackage{lipsum}
\usepackage[english,serbian]{babel}
\usepackage[strings]{underscore}
\usepackage{algorithmic}
\usepackage{amsmath}
%\usepackage[english,serbianc]{babel} %ukljuciti babel sa ovim opcijama, umesto gornjim, ukoliko se koristi cirilica

\usepackage[unicode]{hyperref}
\hypersetup{colorlinks,citecolor=green,filecolor=green,linkcolor=blue,urlcolor=blue}

%\newtheorem{primer}{Пример}[section] %ćirilični primer
\newtheorem{primer}{Definicija}[section]
\newtheorem{lema}{Lema}[section]

\usepackage{listings}
\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}


\begin{document}

\title{Izračunavanje Frešeovog rastojanja između prostih poligona u polinomijalnom vremenu\\\small{Geometrijski algoritmi}}

\author{Andrija Novaković, 1016/2020\\ akinovak@gmail.com}
\maketitle

\abstract{
Autori ovog rada su Kevin Bučin (\textit{eng.} Kevin Buchin), docent na tehničkom univerzitetu Endhoven (\textit{eng.} Eindhoven), na katedri za računarstvo i informatiku, Maika Bučin (\textit{eng.} Maike Buchin), profesor na univerzitetu Bohum (\textit{eng.} Bochum), katedra za teorijsku matematiku i Karola Venk (\textit{eng.} Carola Wenk) profesor na Tulane (\textit{eng.} Tulane) univerzitetu, katedra za računarstvo i informatiku. 

Rad je objavljen u časopisu \textit{Computational Geometry} 2008. godine pod nazivom \textit{Computing the Fréchet distance between simple polygons}\cite{BUCHIN20082}.
Autori su prezentovali prvi algoritam za računanje Frešeovog rastojanja u polinomijalnom vremenu za proste poligone, u potencijalno različitim ravnima. Korišćene su tehnike dinamičkog programiranja i nalaženje najkraćih puteva unutar prostih poligona.

}

\tableofcontents

\newpage

\section{Uvod}
\label{sec:uvod}

Algoritmi prepoznavanja oblika (\textit{eng.} Shape matching algorithms) se baziraju na merenju različitosti oblika računanjem rastojanja razmatranih oblika. Koriste se u najrazličitijim granama tehnološke industrije kao što su: robotika, računarski vid (\textit{eng.} computer vision) i računanje trajektorija. 

\subsection{Neformalan opis}

    Zamislimo situaciju gde osoba obilazi konačanu zakrivljenu stazu i na povocu šeta psa, koji takođe obilazi svoju zakrivljenu konačanu stazu. I čovek i pas mogu da menjaju brzine kako bi povodac ostao zategnut, ali ne mogu da se vraćaju unazad. Frešeova udaljenost je dužina najkraćeg povoca da i čovek i pas predju svoje putanje.
    

\subsection{Formalna definicija}
Krive i funkcije su obično parametrizovane neprekidnim funkcijama $f:A \longrightarrow R^d$ za neko $A \subseteq R^k$. Neka su $f:A \longrightarrow R^d$ i  $g:B \longrightarrow R^d$ dve parametrizovane krive ili površi na homeomorfnim prostorima A i B. Frešeovo rastojanje između njih se definiše kao:
$$\delta_(f, g) = \underset{\sigma: A \longrightarrow B}{inf} sup || f(x) - g(\sigma(x))||$$
Gde $\sigma$ prolazi kroz sve homeomorfizme koji čuvaju orjentaciju, a $||*||$ predstavlja Euklidsko rastojanje. 
\section{Osnovne definicije}
\subsection{Najkraći putevi}
Ako su S1 i S2 dva disjunktna segmenta na poligonu, peščani sat (\textit{eng.} hourglass) od S1 i S2 predstavlja sve najkraće puteve između bilo koje  tačke $p1 \in S1$ i $p2 \in S2$. Što za rezultat daje (moguće degenerisani) poligon.  

\begin{figure}[h!]
\begin{center}
\includegraphics[scale=0.3]{hourglass.png}
\end{center}
\caption{Peščani sat u prostom poligonu}
\label{fig:stanja}
\end{figure}

\subsection{Konveksna dekompozicija}
Za računanje Frešeovog rastojanja između prostih poligona se koristi dekompozicija poligona na konveksne delove. Vremenska složenost ove dekompozicije je $O(n + r^2min(n, r^2))$ gde je n broj čvorova, a r broj unutrašnjih čvorova sa uglom manjim od $\pi$ \cite{article1} \cite{1675001}. 
Ovaj postupak se zove minimalna konveksna dekompozicija (\textit{eng.} minimum convex decomposition).
\subsection{Restrikcija homeomorfizama}
Ključan deo u poboljšavanju algoritma dostiže se restrikcijom homeomorfizama.
To su homeomorfizmi nad granicama P i Q i preslikavanjima dijagonala iz konveksne dekompozicije poligona P u najkraće puteve u poligonu Q. Neka je C konveksna dekompozicija od P. $E_c$ je skup svih tačaka koje leže na nekoj od grana u C, tada važi tvrđenje.
\begin{lema}
Neka su P i Q dva prosta poligona, C konveksna dekompozicija od P i neka $\sigma{'} : E_c \longrightarrow Q$ prolazi kroz sve mape najkraćih puteva koje čuvaju orjentaciju . Tada je Frešeovo rastojanje između P i Q $$\underset{\sigma{'}: Ec \longrightarrow Q}{inf} \underset{t \in E_c}{max} || t - \sigma{'}(t)||$$.
\end{lema}





\section{Izračunavanje}

Dijagram slobodnog prostora između dve krive za dat prag $\epsilon$ je dvodimenzioni region koji se sastoji od svih parova tačaka na dvema krivama koje su na rastojanju najviše $\epsilon$.


\begin{lema}
Frešeovo rastojanje između dva prosta poligona je manje ili jednako $\epsilon$ akko postoji dostižan put u dijagramu slobodnog prostora $F_\epsilon$.
\end{lema}

Za potrebe izračunavanja uvodi se algoritam odlučivanja. Svrha ovog algoritma je da za date proste poligone P, Q i rastojanje epsilon izračuna da li postoji dostižan put u dijagramu slobodnog prostora od P i Q. Koristi kombinovani graf dostupnosti koji je nastao kao unapređenjem strukture dostupnosti \cite{article}. 


Algoritam za izračunavanje se izvršava u vremenu $O(kT(mn)\log(mn))$ gde je T(N) vreme potrebno za množenje matrica $N \times N$, n i m su redom brojevi temena poligona P i Q i k je veličina nastala minimalnom konveksnom dekompozicijom. Prostorna složenost algoritma je $O(k(mn)^2)$  Sastoji se iz tri koraka:
\begin{itemize}
    \item \textbf{Računanje skupa kritičnih vrednosti} - Za neko $\epsilon$ se kaže da je kritična vrednost akko postoji dostižan put u slobodnom prostoru i za svako $\epsilon{'} < \epsilon$ takav put ne postoji. 
    Skup kritičnih vrednosti se može izračunati u  $O(m^2n + m*n^2)$ vremenskoj složenosti \cite{article}. 
    \item \textbf{Sortiranje kritičnih vrednosti}
    \item \textbf{Binarna pretraga kritičnih vrednosti koristeći algoritam\\ odlučivanja} - Algoritam odlučivanja se izvršava u vremenu $O(Tk(mn))$.
\end{itemize}


\section{Zaključak}
Prezentovan je prvi algoritam u polinomijalnom vremenu za izračunavanje Frešeovog rastojanja nad prostim poligonima. Pokazali su da se prostor svih homeomorfizama može svesti na skup "lepih" homeomorifi-zama do kojih se može doći algoritamskim putem. Istraživanje nas približava odgovoru na pitanje da li je Frešeovo rastojanje između površi izračunljivo. Do odgovora se može doći jedino ako se skup homeomorfizama nad površima takođe može restrikovati kao u slučaju prostih poligona. U suprotnom, ako svi homeomorfizmi moraju biti ispitani, sa sigurnošču možemo tvrditi da ovaj problem nije izračunjiv.
Sledeći koraci u istraživanju podrazumevaju proširivanje na opštije klase poligona, kao što su poligoni sa rupama ili preklapanjima.
\addcontentsline{toc}{section}{Literatura}
\appendix
\bibliography{seminarski} 
\bibliographystyle{plain}

\appendix


\end{document}
