% !TEX encoding = UTF-8 Unicode

\documentclass[a4paper]{article}

\usepackage{color}
\usepackage{url}
\usepackage[T2A]{fontenc} % enable Cyrillic fonts
\usepackage[utf8]{inputenc} % make weird characters work
\usepackage{graphicx}

\usepackage[english,serbian]{babel}
%\usepackage[english,serbianc]{babel} %ukljuciti babel sa ovim opcijama, umesto gornjim, ukoliko se koristi cirilica

\usepackage[unicode]{hyperref}
\hypersetup{colorlinks,citecolor=green,filecolor=green,linkcolor=blue,urlcolor=blue}

%\newtheorem{primer}{Пример}[section] %ćirilični primer
\newtheorem{primer}{Primer}[section]

\usepackage{amssymb}
\usepackage{listings}
\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}

\lstset{ 
  backgroundcolor=\color{white},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}; should come as last argument
  basicstyle=\scriptsize\ttfamily,        % the size of the fonts that are used for the code
  breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
  breaklines=true,                 % sets automatic line breaking
  captionpos=b,                    % sets the caption-position to bottom
  commentstyle=\color{mygreen},    % comment style
  deletekeywords={...},            % if you want to delete keywords from the given language
  escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
  extendedchars=true,              % lets you use non-ASCII characters; for 8-bits encodings only, does not work with UTF-8
  firstnumber=1000,                % start line enumeration with line 1000
  frame=single,	                   % adds a frame around the code
  keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
  keywordstyle=\color{blue},       % keyword style
  language=Python,                 % the language of the code
  morekeywords={*,...},            % if you want to add more keywords to the set
  numbers=left,                    % where to put the line-numbers; possible values are (none, left, right)
  numbersep=5pt,                   % how far the line-numbers are from the code
  numberstyle=\tiny\color{mygray}, % the style that is used for the line-numbers
  rulecolor=\color{black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
  showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
  showstringspaces=false,          % underline spaces within strings only
  showtabs=false,                  % show tabs within strings adding particular underscores
  stepnumber=2,                    % the step between two line-numbers. If it's 1, each line will be numbered
  stringstyle=\color{mymauve},     % string literal style
  tabsize=2,	                   % sets default tabsize to 2 spaces
  title=\lstname                   % show the filename of files included with \lstinputlisting; also try caption instead of title
}

\begin{document}

\title{Problemi sa trkačima na stazi\\ \small{Seminarski rad u okviru kursa\\Geometrijski algoritmi\\ Matematički fakultet}}

\author{Petar Zečević, 1046/2020}
\maketitle

\abstract{

Autori ovog rada su definisali nekoliko problema vezanih za trkače na stazi. Svi problemi imaju narednu premisu: imamo kružnicu C dužine 1 i kružni luk A na njoj dužine $l \in (0,1)$. Takođe imamo $k$ trkača koji trče po kružnici različitim, konstantnim i pozitivnim brzinama i kreću sa različitih (u nekim problemima sa istih) pozicija na kružnici. Prikazano je da za bilo koje vreme $t \geq 0$ postoji raspored početnih pozicija trkača i njihovih brzina tako da bar jedan trkač nije u kružnom luku $A$ u trenutku $t$. 

Rad je objavljen u junu 2020. godine u časopisu ''Computational Geometry (volume 88)'' pod imenom ''Problems on track runners''. Autori rada su: Adrian Dumitrescu, Department of Computer Science, University of Wisconsin–Milwaukee, Milwaukee, WI, USA i Csaba D. T\'{o}th, Department of Mathematics, California State University, Northridge, Los Angeles, CA, USA. Preliminarna verzija rada je objavljena u ''Proceedings of the 29th Canadian Conference on Computational Geometry'', Ottawa, ON, Canada, July 2017.}

\tableofcontents

\newpage

\section{Uvod}
\label{sec:uvod}
Wills \cite{Wills} i Cusick  \cite{Cusick} su definisali problem usamljenog trkača. k trkača počinju iz iste tačke i imaju konstante i različite brzine. Trkač je usamljen kada je udaljen od ostalih trkača za bar $\frac{1}{k}$ (pod udaljenost se podrazumeva dužina dela kružnice između dva trkača). Tvrđenje je da svaki trkač ima trenutak u vremenu kad je usamljen. Dokazano je za do $7$ trkača.

Pojavljuje se i nekoliko problema koji su vezani za patroliranje agenata \cite{Chevaleyre, DATC, KK}. Imamo $k$ agenata koji imaju (ne nužno) različite maksimalne brzine i patroliraju zatvorenom ili otvorenom ogradom (u obliku kružnice ili duži). Njihovo kretanje tokom vremena je opisano rasporedom patroliranja. Svaki agent se kreće promenljivom brzinom, ograničenom sa $0$ i njegovom maksimalnom brzinom, u bilo kom smeru ograde (odnosno kružnice). Cilj je naći raspored patroliranja takav da minimizuje vreme čekanja (najduže vreme za koje jedan deo kružnice nije posećen ni od koga). U toj oblasti je ostalo nekoliko otvorenih pitanja: ako nam je dato $t$, da li možemo da osiguramo da će maksimalno vreme čekanja biti manje od $t$?

Rad koji je ovde prezentovan \cite{trackrunners} se bavi nekim od problema vezanih za trkače i unapređenje je rada istih autora iz 2014. godine \cite{DATC2}. U tom radu je postavljeno sledeće pitanje:

\textbf{Pitanje 1}. Pretpostavimo da $k$ trkača konstantnih i međusobno različitih brzina, trče u smeru kazaljke na satu po kružnici dužine $1$, krećući sa proizvoljnih mesta. Pretpostavimo takođe da je neki povezan deo trake sve vreme u senci. Da li uvek postoji vreme kada su svi trkači na delu trake koji je u senci?

Odgovor je da može da se napravi takav raspored brzina trkača da ovo ne važi, čak i da je deo trake koji je pod senkom dužine $0.99999$. Naravno, podrazumeva se da je broj trkača dovoljno veliki.

U sekciji 2 su pokazani neki problemi sa trkačima dati preko teorema. Te teoreme se mogu naći i u \cite{DATC2}. Ovaj rad \cite{trackrunners} daje svoj doprinos u obliku dva algoritma koji su ovde pokazani u sekciji 3.

\section{Trkači u senci}

Podrazumeva se da svi trkači trče u istom smeru i da počinju u istom trenutku. Sledi nekoliko teorema koje govore o problemima trkača u senci:

\textbf{Teorema 1}. Imamo kružnicu sa kružnim lukom $A$. Postoji raspored za $k$ trkača sa različitim i konstantim brzinama i proizvoljnim početnim tačkama takav da za bilo koje $t$, bar jedan trkač nije u $A$.

Jedan način da se napravi raspored za koji ovo važi je:
Trkač 1 počinje u tački $0$ u vremenu $0$. U vremenu $a$ ($a$ je dužina od $A$) taj trkač je u tački a (što znači da izlazi iz podintervala). Trkač 2 je u tački $0$ u vremenu $a$, u vremenu $a + \frac{a}{2}$, nalazi se u tački a. Trkač 3 je u tački 0 u vremenu $a + \frac{a}{2}$, a u vremenu $a + \frac{a}{2} + \frac{a}{3}$, nalazi se u a. Ovo se nastavi za ostale trkače.

Ova teorema se proširuje na konačan broj podintervala. U radu je prikazan i jedan zanimljiv slučaj gde je dokazana suprotna tvrdnja pod određenim uslovima.

\textbf{Teorema 2}. Imamo $k$ trkača sa konstantnim i racionalno nezavisnim brzinama koji kreću iz proizvoljnih tačaka na kružnici. Za svaki luk $A$ i svako $T > 0$ postoji $t > T$ tako da su svi trkači u luku $A$.

Zanmljivo je da Teorema 1 važi i za mnogo velike lukove a Teorema 2 važi i za mnogo male. Za brojeve za koje važi da su racionalno nezavisni, važi da bar jedan mora da bude iracionalan. Zapravo se ispostavlja da je uslov iz Teoreme 2 da su brzine  racionalno nezavisne dovoljan ali ne i neophodan. To se tvrdi u Teoremi 3.

\textbf{Teorema 3}. Za svaki luk $A$ i date početne pozicije trkača, postoje različite i racionalne brzine takve da važi: za svako $T > 0$ postoji $t > T$ takvo da su u tom momentu svi trkači u $A$.

Brzine možemo uzeti na sledeći način: $v_k = \lceil\frac{2}{a}v_{k-1}\rceil$(a je dužina luka). Pokazuje se da za date početne pozicije, Teorema 5 važi za sve lukove iste dužine. Odnosno, za sve lukove dužine a (luk može da počinje bilo gde), za isti raspored početnih pozicija, važi tvrdnja iz Teoreme 5.


\section{Algoritmi}

U radu su prikazana dva algoritma koja određuju da li za date parametre postoji trenutak kada su svi trkači u senci. Prvi algoritam je dat kroz teoremu i njen dokaz.

\textbf{Teorema 4}. Dato je k trkača sa racionalnim i različitim brzinama $v_1, ..., v_k$ koji kreću od 0, i dat je luk kružnice A. Postoji algoritam za određivanje postojanja broja $t > 0$ tako da je svih k trkača u vremenu t u luku A. Takođe postoji algoritam koji odlučuje da li za dato $T > 0$ postoji $t > T$ tako da su svi trkači u A u vremenu t. Vremenska složenost algoritma je $O(k^2r_k)$ ($r_i$ su brojioci brzina, a $r_k$ je najveći od njih).

Dokaz. Prvo pretpostavimo da su $v_1, v_2, ..., v_k$ celi brojevi. Pošto je dužina trake 1, to znači da je svaka brzina umnožak dužine trake, a samim tim da je perioda svih trkača 1. To znači da je dovoljno da gledamo samo vremenski interval $[0,1)$. Za $i = 1,2, ..., k$ definišemo $I_i$ kao skup vremenskih intervala iz $[0,1)$ u kojima je trkač i u luku A. Definišimo takođe $K_i$ kao skup vremenskih intervala iz $[0,1)$ u kojima su svi trkači od 0 do i u A. Uočimo da $I_i$ sadrži $v_i$ intervala iste dužine. Važi da je $|K_i| < \sum^i_{j=1}v_j$ 
Imamo da je $I_1$ jednako $K_1$. $I_i$ računamo kao: $$ I_i = \{\frac{1}{v_i}(h + A) : h = 0, 1, ..., v_i - 1\}$$
$K_i$ se računaju kao preseci $I_i$ i $K_{i-1}$. Vremenska složenost i-tog koraka je $O(iv_i)$, a ukupna složenost je $O(k^2v_k)$ (pretpostavimo da je $v_k$ maksimalna brzina).

Sada uzmimo da su brzine racionalni brojevi $v_i = \frac{r_i}{q}$ gde je q najmanji zajednički delilac svih $v_i$. Pošto je svaka brzina umnožak od $\frac{1}{q}$, brzine su periodične sa periodom q, tako da ćemo analizirati samo vremenski interval $[0,q)$. Računamo $I_i$ i $K_i$ slično kao za cele brojeve, s malom izmenom za $I_i$: $$ I_i = \{\frac{q}{r_i}(h + A) : h = 0, 1, ..., r_i - 1\}$$
Pošto je $|I_i| = ri$ i $|K_i| < \sum^1_{j=1}r_j$ . vremenska složenost za i-ti korak je $O(ir_i)$, a ukupna vremenska složenost je $O(k^2r_k)$ (pretpostavimo da je $r_k$ najveći $r_i$). $\square$

Drugi algoritam je veoma sličan kao prvi. Jedina razlika je što trkači mogu da kreću sa proizvoljnog mesta (ne nužno od tačke 0).

Ovi algoritmi mogu naći svoju primenu u planiranju saobraćaja. Recimo da imamo neku obilaznicu oko grada (na primer M25 oko Londona) i želimo da ispitamo opterećenost određenih delova te obilaznice. Mogli bismo da napravimo neku alternativu gorenavedenih algoritama da bismo to uradili.

Sledeći korak bi mogao biti nalaženje sličnih algoritama. Jedan primer je algoritam koji za date brzine trkača i njihove početne pozicije, i dužinu $l > 0$ i vreme $T > 0$, proverava da li postoji kružni luk $A$ dužine $l$ takav da je za vreme $t > T$ bar jedan trkač u $A$.

\addcontentsline{toc}{section}{Literatura}
\appendix
\bibliography{literatura} 
\bibliographystyle{plain}



\end{document}
